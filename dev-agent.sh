#! /bin/bash
sudo apt update -y 
sudo apt install git -y 
sudo apt install  software-properties-common gnupg2 curl -y 
curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg
sudo install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/
echo -ne '\n' | sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main" ||
echo -ne '\n' | return
sudo apt install terraform
terraform --version
sudo apt install ansible -y 